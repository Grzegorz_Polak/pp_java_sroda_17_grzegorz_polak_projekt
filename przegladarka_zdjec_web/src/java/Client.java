
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Grzegorz
 */
public class Client {
    
    Socket s;
    DataOutputStream dos = new DataOutputStream(s.getOutputStream());
    String imgPath = "E:/rozne rzeczy/NetBeans projekty/Images/Iceland.jpg";
    File file = new File(imgPath);
    byte[] b = new byte[(int) file.length()];
    FileInputStream fis;

    public Client() {
        try {
            this.s = new Socket("localhost", 3333);
            this.fis = new FileInputStream(file);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    fis.read(b);
    fis.close();
    dos.writeInt((int) file.length());
    dos.flush();
    dos.write(b, 0, b.length);
    dos.flush();

}
